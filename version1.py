#Word Game 
#Example used: CLAY -> CLAD -> GLAD -> GOAD -> GOLD
import sys

def load_words(file_path):
    with open(file_path, 'r') as file:
        word_list = set(word.strip().upper() for word in file.readlines())
    return word_list

def check_length(start: str, stop: str):
     if (len(start) != len(stop)):
         return "Error"
     return True

#def stop_condition(start: str, stop: str, iterations: list):
    #for x,y in zip(start,stop):
        #return x == y 
        

def convert( start: str, stop: str , word_list: list):
    #if not check_length(start, stop):
        #return "ERROR"
    iterations = [start]
    while start != stop:
        for i in range(len(start)):
            #  curr = start[: i ] + stop[i] + start[i + 1:]
            #  if(curr in  word_list):
            #      iterations.append(curr)
  
            for ch in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
                curr = start[:i] + ch + stop[i+1:]
                if curr ==stop:
                    iterations.append(curr)
                    return iterations
                elif curr in word_list and curr not in iterations:
                    iterations.append(curr)
                    start = curr
                    break
    return "No transformations"               
    

print(convert( sys.argv[1], sys.argv[2], load_words("paradox/sowpods.txt")))
