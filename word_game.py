#Word Game 
#Example used: CLAY -> CLAD -> GLAD -> GOAD -> GOLD

from collections import deque

def load_words(file_path):
    with open(file_path, 'r') as file:
        words = set(word.strip().upper() for word in file.readlines())
    return words


def neighbour_words(word, dictionary):
    neighbors = []
    for i in range(len(word)):
        for c in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
            if c != word[i]:
                new_word = word[:i] + c + word[i+1:]
                if new_word in dictionary:
                    print(new_word)
                    neighbors.append(new_word)
    return neighbors
